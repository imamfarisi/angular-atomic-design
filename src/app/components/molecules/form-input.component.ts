import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: 'form-input',
    template: `
        <app-label 
            [for]="id" [label]="label" [required]="required">
        </app-label>
        <app-input 
            [type]="type" [name]="name" [id]="id" 
            [data]="data" (dataChange)="onDataChange($event)" [className]="className" 
            [required]="required" [maxlength]="maxlength" [minlength]="minlength">
        </app-input>
    `,
})
export class FormInputComponent {
    //input
    @Input() type: string = "text";
    @Input() name!: string;
    @Input() id!: string;

    @Input() data?: any;
    @Output() dataChange = new EventEmitter<any>();

    @Input() className: string = '';
    @Input() required: boolean = false;
    @Input() maxlength?: number;
    @Input() minlength?: number;

    //label
    @Input() label: string = "";

    onDataChange(data: any): void {
        this.dataChange.emit(data);
    }
}