import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ButtonComponent } from "./atoms/button.component";
import { InputComponent } from "./atoms/input.component";
import { LabelComponent } from "./atoms/label.component";
import { FormInputComponent } from "./molecules/form-input.component";

@NgModule({
    declarations: [
        InputComponent, LabelComponent, ButtonComponent,
        FormInputComponent
    ],
    imports: [
        CommonModule, FormsModule
    ],
    exports: [
        InputComponent, LabelComponent, ButtonComponent,
        FormInputComponent
    ]
})
export class ComponentModule { }