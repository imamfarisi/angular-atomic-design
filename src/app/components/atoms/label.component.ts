import { Component, Input } from "@angular/core";

@Component({
    selector: 'app-label',
    template: `
         <label for="for" class="form-label">
            {{label}}<span *ngIf="required" class="text-danger">*</span>
         </label>
    `
})
export class LabelComponent {

    @Input() for?: string;
    @Input() label: string = "";
    @Input() required: boolean = false;

}