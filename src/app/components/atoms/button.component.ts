import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
    selector: 'app-button',
    template: `
         <button 
            [type]="type" [id]="id" class="btn {{className}}" 
            (click)="onClick()" [disabled]="disabled">
                {{label}}
        </button>
    `
})
export class ButtonComponent {

    @Input() id!: string;
    @Input() type: string = 'button';
    @Input() className: string = '';
    @Input() disabled: boolean = false;
    @Input() label: string = "";

    @Output('onClick') click = new EventEmitter<void>();

    onClick(): void {
        this.click.emit();
    }

}