import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ControlContainer, NgForm } from "@angular/forms";

@Component({
    selector: 'app-input',
    template: `
        <input 
            [type]="type" [name]="name" [id]="id" 
            class="form-control {{className}}" [(ngModel)]="data" (ngModelChange)="changeData($event)" 
            [required]="required" [maxlength]="maxlength || null" [minlength]="minlength || null">
    `,
    viewProviders: [{ provide: ControlContainer, useExisting: NgForm }]
})
export class InputComponent {

    @Input() data?: any;
    @Input() type: string = "text";
    @Input() name!: string;
    @Input() id!: string;
    @Input() className: string = '';
    @Input() required: boolean = false;
    @Input() maxlength?: number;
    @Input() minlength?: number;
    @Output() dataChange = new EventEmitter<any>();

    changeData(data: any): void {
        this.dataChange.emit(data);
    }
}