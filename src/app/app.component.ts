import { Component } from '@angular/core';
import { LoginDto } from './dto/login-dto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  login = new LoginDto();

  click(invalid : boolean): void {
    if(!invalid) {
      //do your click
      console.log('cliccccled')
    }
  }
}

